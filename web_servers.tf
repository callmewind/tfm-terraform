data template_file "wordpress_config" {
 template = file("wp-config.php")
 vars = {
  wordpress_db_host = var.galera_lb_private_ip
  wordpress_db_name = var.wordpress_db_name
  wordpress_db_user = var.wordpress_db_user
  wordpress_db_password = var.wordpress_db_password
 }
}


data template_file "nginx_config" {
 count = var.webservers
 template = file("wordpress-nginx.conf")
 vars = {
  index               = count.index
 }
}

data template_file "web_userdata" {
 count = var.webservers
 template = file("web_userdata.yml")
 vars = {
  ssh_public_key = hcloud_ssh_key.default.public_key
  wordpress_nginx = jsonencode(data.template_file.nginx_config[count.index].rendered)
  wordpress_config = jsonencode(data.template_file.wordpress_config.rendered)
 }
}


resource "hcloud_server" "web" {
  count       = var.webservers
  name        = "web-server-${count.index}"
  image       = var.os_type
  server_type = var.server_type
  location    = var.location
  ssh_keys    = [hcloud_ssh_key.default.id]
  labels = {
    type = "web"
  }
  user_data = data.template_file.web_userdata[count.index].rendered
}

resource "hcloud_volume" "web_server_volume" {
  count    = var.webservers
  name     = "web-server-volume-${count.index}"
  size     = var.disk_size
  location = var.location
  format   = "xfs"
}

resource "hcloud_volume" "galera_volume" {
  count    = var.galeras
  name     = "galera-server-volume-${count.index}"
  size     = var.disk_size
  location = var.location
  format   = "xfs"
}

resource "hcloud_volume_attachment" "web_vol_attachment" {
  count     = var.webservers
  volume_id = hcloud_volume.web_server_volume[count.index].id
  server_id = hcloud_server.web[count.index].id
  automount = true
}

resource "hcloud_volume_attachment" "galera_vol_attachment" {
  count     = var.galeras
  volume_id = hcloud_volume.galera_volume[count.index].id
  server_id = hcloud_server.galera[count.index].id
  automount = true
}

data template_file "galera_cnf" {
  count = var.galeras
  template = file("galera.cnf")
  vars = {
    current_ip        = "10.0.1.${count.index + 5}"
    current_hostname  = "galera-server-${count.index}"
    galera_ips        = join(",", [for i in range(var.galeras) : format("10.0.1.%d", i + 5)])
  }
}

data template_file "galera_userdata" {
 count = var.galeras
 template = file("galera_userdata.yml")
 vars = {
  index               = count.index
  galera_cnf          = jsonencode(data.template_file.galera_cnf[count.index].rendered)
  ssh_public_key      = hcloud_ssh_key.default.public_key
  wordpress_db_name   = var.wordpress_db_name
  wordpress_db_user   = var.wordpress_db_user
  wordpress_db_password = var.wordpress_db_password
 }
}

resource "hcloud_server" "galera" {
  count       = var.galeras
  name        = "galera-server-${count.index}"
  image       = var.os_type
  server_type = var.server_type
  location    = var.location
  ssh_keys    = [hcloud_ssh_key.default.id]
  labels = {
    type = "galera"
  }
  user_data = data.template_file.galera_userdata[count.index].rendered
}
